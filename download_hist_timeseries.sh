#!/bin/bash

echo Fetching data for historical timeseries from PIK server.

run=set rsync --password-file=rsync_pik c2p2_user@rsync.pik-potsdam.de::c2p2/August2021_experiment/lpjml-results/hist_timeseries/hist_timeseries.nc output/hist_timeseries.nc
exit $?
