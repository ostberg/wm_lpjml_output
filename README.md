# World Modelers LPJmL model for raw simulation outputs

This project contains code to ingest LPJmL crop yield forecasts and historical yield simulations into the World Modelers "Dojo" system. This model returns raw, gridded simulation outputs that are not aggregated spatially or temporally.

## Historical yield simulations
`download_hist_timeseries.sh` fetches pre-computed results of the historical yield simulation from the PIK server.

## Crop yield forecasts
`download_forecast.sh` fetches pre-computed results of crop yield forecast simulations from the PIK server.
### Input knob settings
The following input knobs are available:
- fertilizer_scenario
- irrigation_scenario
- sowing_scenario
- weather_year

Input knob settings are passed as command line arguments to `download_forecast.sh`. Example call: `download_forecast.sh -fertilizer_scenario reference -irrigation_scenario reference -sowing_scenario reference -weather_year 1984-1985`

## Output
`download_hist_timeseries.sh` deposits a file `output/hist_timeseries.nc`
`download_forecast.sh` deposits a file `output/forecast_2021-2022.nc`
