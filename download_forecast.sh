#!/bin/bash

# set defaults if missing command line parameters
fertilizer_scenario="reference"
sowing_scenario="reference"
irrigation_scenario="reference"
weather_year="1984-1985"

## set valid options
fertilizer_options=( "reference" "refP25" "refP50" "refP100" )
sowing_options=( "minus30" "minus15" "reference" "plus15" "plus30" )
irrigation_options=( reference )
weather_options=( "1984-1985" "1985-1986" "1986-1987" "1987-1988" "1988-1989" "1989-1990" "1990-1991" "1991-1992" "1992-1993"
                  "1993-1994" "1994-1995" "1995-1996" "1996-1997" "1997-1998" "1998-1999" "1999-2000" "2000-2001" "2001-2002"
                  "2002-2003" "2003-2004" "2004-2005" "2005-2006" "2006-2007" "2007-2008" "2008-2009" "2009-2010" "2010-2011"
                  "2011-2012" "2012-2013" "2013-2014" "2014-2015" "2015-2016" "2016-2017" "2017-2018" "2018-2019" "2019-2020" )

# read input parameters from command line
while(( "$#" )); do
  case "$1" in
    -fertilizer_scenario)
      if [ $# -lt 2 ]; then
        echo >&2 value for fertilizer_scenario missing
        exit 1
      fi
      fertilizer_scenario=$2
      shift 2
      ;;
    -sowing_scenario)
      if [ $# -lt 2 ]; then
        echo >&2 value for sowing_scenario missing
        exit 1
      fi
      sowing_scenario=$2
      shift 2
      ;;
    -irrigation_scenario)
      if [ $# -lt 2 ]; then
        echo >&2 value for irrigation_scenario missing
        exit 1
      fi
      irrigation_scenario=$2
      shift 2
      ;;
    -weather_year)
      if [ $# -lt 2 ]; then
        echo >&2 value for weather_year missing
        exit 1
      fi
      weather_year=$2
      shift 2
      ;;
    -*)
      echo >&2 Invalid option $1
      exit 1
      ;;
    *)
      echo >&2 Invalid argument $1
      exit 1
      ;;
  esac
done

fert_valid=0
for opt in ${fertilizer_options[@]}; do
  if [ "$fertilizer_scenario" == "$opt" ]; then
    fert_valid=1
  fi
done
if [ ${fert_valid} -lt 1 ]; then
  echo >&2 Invalid fertilizer_scenario $fertilizer_scenario
  exit 1
fi
sowing_valid=0
for opt in ${sowing_options[@]}; do
  if [ "$sowing_scenario" == "$opt" ]; then
    sowing_valid=1
  fi
done
if [ ${sowing_valid} -lt 1 ]; then
  echo >&2 Invalid sowing_scenario $sowing_scenario
  exit 1
fi
irrig_valid=0
for opt in ${irrigation_options[@]}; do
  if [ "$irrigation_scenario" == "$opt" ]; then
    irrig_valid=1
  fi
done
if [ ${irrig_valid} -lt 1 ]; then
  echo >&2 Invalid irrigation_scenario $irrigation_scenario
  exit 1
fi
weather_valid=0
for opt in ${weather_options[@]}; do
  if [ "$weather_year" == "$opt" ]; then
    weather_valid=1
  fi
done
if [ ${weather_valid} -lt 1 ]; then
  echo >&2 Invalid weather_year $weather_year
  exit 1
fi

if [ ${#sowing_scenario} -lt 5 ]; then
  echo >&2 Invalid sowing_scenario $sowing_scenario
  exit 1
fi

if [ "$irrigation_scenario" != "reference" ]; then
  echo >&2 Invalid irrigation_scenario $irrigation_scenario
  exit 1
fi

if [ ${#weather_year} -ne 9 ]; then
  echo >&2 Invalid weather_year $weather_year
  exit 1
fi

echo Fetching data for irrigation_scenario \"$irrigation_scenario\", fertilizer_scenario \"$fertilizer_scenario\", sowing_scenario \"$sowing_scenario\", and weather_year \"$weather_year\" from PIK server.

run=set rsync --password-file=rsync_pik c2p2_user@rsync.pik-potsdam.de::c2p2/August2021_experiment/lpjml-results/forecast_2021-2022/irrigation_target_${irrigation_scenario}_n_rate_shift_${fertilizer_scenario}_sowing_delta_${sowing_scenario}/forecast_2021-2022_wy${weather_year}.nc output/forecast_2021-2022.nc
exit $?
